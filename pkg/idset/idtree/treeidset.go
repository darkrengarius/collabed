package idtree

import (
	"encoding/json"
	"math"
	"sync"

	"gitlab.com/darkrengarius/collabed/pkg/op"

	"gitlab.com/darkrengarius/collabed/pkg/idset"
)

// TreeIDSet is a dense set of globally unique ordered identifiers. A way in a tree
// from root to the node forms an ID of this node. If we take turn left, we add 0 to the ID,
// 1 - otherwise. If some clients generate equal IDs, these get ordered with the help of
// Lamport timestamp. In this implementation nodes do not get removed from the tree,
// they just get marked as tombstones.
type TreeIDSet struct {
	mx     sync.Mutex
	root   *Node
	idsMap *orderedMap
}

func (s *TreeIDSet) MarshalJSON() ([]byte, error) {
	str := struct {
		Root *Node
		Buf  *idList
	}{
		Root: s.root,
		Buf:  s.idsMap.list,
	}

	return json.Marshal(&str)
}

func (s *TreeIDSet) UnmarshalJSON(data []byte) error {
	var str struct {
		Root *Node
		Buf  *idList
	}

	if err := json.Unmarshal(data, &str); err != nil {
		return err
	}

	s.root = str.Root

	s.idsMap = newOrderedMap()
	s.idsMap.list = str.Buf
	s.idsMap.m = make(map[string]*idListNode)

	for h := s.idsMap.list.h; h != nil; h = h.next {
		s.idsMap.m[h.id.Key] = h
	}

	return nil
}

func (s *TreeIDSet) Export() []idset.ID {
	s.mx.Lock()
	defer s.mx.Unlock()

	return s.idsMap.list.export()
}

func Import(ids []idset.ID) *TreeIDSet {
	s := NewTreeIDSet()

	for _, id := range ids {
		s.insert(id)
	}

	return s
}

func NewTreeIDSet() *TreeIDSet {
	return &TreeIDSet{
		root:   &Node{},
		idsMap: newOrderedMap(),
	}
}

func NewTreeIDSetInit(n int) *TreeIDSet {
	s := NewTreeIDSet()
	if n == 0 {
		return s
	}

	l := math.Ceil(math.Log2(float64(n)))
	h := int(l)
	s.root.init(n, 0, h, 0)

	ids := s.root.export([]idset.ID{}, []idset.IDToken{}, "")
	for _, id := range ids {
		s.idsMap.add(id)
	}

	return s
}

func (s *TreeIDSet) Replay(o op.Op) {
	s.mx.Lock()
	defer s.mx.Unlock()

	switch o.Type {
	case op.TypeInsert:
		//fmt.Printf("REPLAYING INSERT %v\n", o)
		s.insert(o.ID)
		//fmt.Printf("DONE REPLAYING INSERT %v\n", o)
	case op.TypeDelete:
		//fmt.Printf("REPLAYING DELETE: %v\n", o)
		s.remove(o.ID)
		//fmt.Printf("DONE REPLAYING DELETE: %v\n", o)
	}
}

func (s *TreeIDSet) insert(id idset.ID) {
	id.Key = id.String()

	s.root.insert(id, 0)
	s.idsMap.addOrUpdateVal(id)
}

func (s *TreeIDSet) RandomDelete(rnd *Rand) idset.ID {
	s.mx.Lock()
	defer s.mx.Unlock()

	if s.idsMap.len() == 0 {
		return idset.EmptyID
	}

	if s.idsMap.allTombstones() {
		return idset.EmptyID
	}

	var id idset.ID
	for len(id.Tokens) == 0 {
		idx := rnd.Intn(s.idsMap.len())
		i := 0
		for h := s.idsMap.list.h; h != nil; h = h.next {
			if i == idx {
				if h.id.IsTombstone {
					return idset.EmptyID
				}

				id = h.id
				break
			}

			i++
		}
	}

	if id.IsEmpty() {
		return idset.EmptyID
	}

	s.remove(id)

	return id
}

func (s *TreeIDSet) Equal(s2 *TreeIDSet) bool {
	return s.root.equal(s2.root)
}

func (s *TreeIDSet) RandomUpdate(rnd *Rand, dis string) (idset.ID, idset.ID) {
	s.mx.Lock()
	defer s.mx.Unlock()

	if s.idsMap.len() == 0 {
		return idset.EmptyID, idset.EmptyID
	}

	if s.idsMap.allTombstones() {
		return idset.EmptyID, idset.EmptyID
	}

	val := int32(rnd.Intn(math.MaxInt32))

	var id idset.ID
	for len(id.Tokens) == 0 {
		idx := rnd.Intn(s.idsMap.len())
		i := 0
		for h := s.idsMap.list.h; h != nil; h = h.next {
			if i == idx {
				if h.id.IsTombstone {
					return idset.EmptyID, idset.EmptyID
				}
				id = h.id
				break
			}
		}
	}

	return id, s.update(id, val, dis)
}

func (s *TreeIDSet) RandomInsert(rnd *Rand, dis string) idset.ID {
	s.mx.Lock()
	defer s.mx.Unlock()

	val := rnd.Intn(math.MaxInt32)

	if s.idsMap.len() == 0 {
		return s.newID(int32(val), idset.EmptyID, idset.EmptyID, dis)
	}

	if s.idsMap.len() == 1 {
		return s.newID(int32(val), s.idsMap.first(), idset.EmptyID, dis)
	}

	lIdx := rnd.Intn(s.idsMap.len())
	if lIdx == 0 {
		lNode := s.idsMap.firstNode()
		return s.newID(int32(val), lNode.id, lNode.next.id, dis)
	}

	if lIdx == (s.idsMap.len() - 1) {
		var last *idListNode
		for h := s.idsMap.list.h; h != nil; h = h.next {
			last = h
		}

		return s.newID(int32(val), last.prev.id, last.id, dis)
	}

	idx := 0
	for h := s.idsMap.list.h; h != nil; h = h.next {
		if idx == lIdx {
			return s.newID(int32(val), h.id, h.next.id, dis)
		}
		idx++
	}

	return idset.EmptyID
}

func (s *TreeIDSet) NewID(val int32, l, r idset.ID, dis string) idset.ID {
	s.mx.Lock()
	defer s.mx.Unlock()

	return s.newID(val, l, r, dis)
}

func (s *TreeIDSet) newID(val int32, l, r idset.ID, dis string) idset.ID {
	id := s.addNewID(val, l, r, dis)
	s.idsMap.add(id)

	return id
}

func (s *TreeIDSet) addNewID(val int32, l, r idset.ID, dis string) idset.ID {
	if len(l.Tokens) == 0 && len(r.Tokens) == 0 {
		s.root = appendMiniNodeOrCreateNode(s.root, val, dis)

		return idset.NewID([]idset.IDToken{
			{
				Tag:           "",
				Disambiguator: dis,
			}}, val)
	}

	if len(l.Tokens) == 0 {
		return s.root.followLeftRoot(val, r, 0, dis, []idset.IDToken{})
	}

	if len(r.Tokens) == 0 {
		return s.root.followRightRoot(val, l, 0, dis, []idset.IDToken{})
	}

	if l.Before(r) {
		return s.root.newID(val, l, r, 0, 0, dis, []idset.IDToken{})
	}

	return s.root.newID(val, r, l, 0, 0, dis, []idset.IDToken{})
}

func (s *TreeIDSet) Remove(id idset.ID) {
	s.mx.Lock()
	defer s.mx.Unlock()

	s.remove(id)
}

func (s *TreeIDSet) remove(id idset.ID) {
	id.IsTombstone = true
	id.Key = id.String()

	s.idsMap.addOrUpdateTombstoneMark(id)
	s.root.remove(id, 0)
}

func (s *TreeIDSet) Update(id idset.ID, newVal int32, dis string) idset.ID {
	s.mx.Lock()
	defer s.mx.Unlock()

	return s.update(id, newVal, dis)
}

func (s *TreeIDSet) update(id idset.ID, newVal int32, dis string) idset.ID {
	l, r := s.idsMap.prevAndNext(id)
	s.remove(id)

	return s.newID(newVal, l, r, dis)
}

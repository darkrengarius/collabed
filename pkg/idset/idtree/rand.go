package idtree

import (
	"math/rand"
	"sync"
	"time"
)

type Rand struct {
	mx  sync.Mutex
	rnd *rand.Rand
}

func NewRand() *Rand {
	return &Rand{
		rnd: rand.New(rand.NewSource(time.Now().UnixNano())),
	}
}

func (r *Rand) Intn(n int) int {
	r.mx.Lock()
	defer r.mx.Unlock()

	return r.rnd.Intn(n)
}

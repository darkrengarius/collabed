package idtree

import "gitlab.com/darkrengarius/collabed/pkg/idset"

type orderedMap struct {
	list *idList
	m    map[string]*idListNode
}

func newOrderedMap() *orderedMap {
	return &orderedMap{
		list: newIDList(),
		m:    make(map[string]*idListNode),
	}
}

func (m *orderedMap) add(id idset.ID) {
	node := m.list.add(id)
	m.m[id.Key] = node
}

func (m *orderedMap) addOrUpdateVal(id idset.ID) {
	node, ok := m.m[id.Key]
	if ok {
		node.id.Val = id.Val
	} else {
		node := m.list.add(id)
		m.m[id.Key] = node
	}
}

func (m *orderedMap) addOrUpdateTombstoneMark(id idset.ID) {
	node, ok := m.m[id.Key]
	if ok {
		node.id.IsTombstone = id.IsTombstone
	} else {
		node := m.list.add(id)
		m.m[id.Key] = node
	}
}

func (m *orderedMap) prevAndNext(id idset.ID) (idset.ID, idset.ID) {
	node, ok := m.m[id.Key]
	if !ok {
		return idset.EmptyID, idset.EmptyID
	}

	l := idset.EmptyID
	if node.prev != nil {
		l = node.prev.id
	}

	r := idset.EmptyID
	if node.next != nil {
		r = node.next.id
	}

	return l, r
}

func (m *orderedMap) firstNode() *idListNode {
	if m.len() == 0 {
		return nil
	}

	return m.list.h
}

func (m *orderedMap) first() idset.ID {
	node := m.firstNode()
	if node == nil {
		return idset.EmptyID
	}

	return node.id
}

func (m *orderedMap) allTombstones() bool {
	for h := m.list.h; h != nil; h = h.next {
		if !h.id.IsTombstone {
			return false
		}
	}

	return true
}

func (m *orderedMap) len() int {
	return m.list.ln
}

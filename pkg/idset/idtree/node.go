package idtree

import (
	"gitlab.com/darkrengarius/collabed/pkg/idset"
)

type MiniNode struct {
	IsTombstone   bool
	Val           int32
	Disambiguator string
	L, R          *Node
}

type Node struct {
	IsTombstone        bool
	MiniNodeTombstones int
	MiniNodes          []*MiniNode
}

func (n *Node) export(ids []idset.ID, tokens []idset.IDToken, tag string) []idset.ID {
	for _, m := range n.MiniNodes {
		tokens = append(tokens, idset.IDToken{
			Tag:           tag,
			Disambiguator: m.Disambiguator,
		})

		idTokens := make([]idset.IDToken, len(tokens))
		copy(idTokens, tokens)
		id := idset.ID{
			Key:         "",
			Tokens:      idTokens,
			Val:         m.Val,
			IsTombstone: m.IsTombstone,
		}
		id.Key = id.String()
		ids = append(ids, id)

		if m.L != nil {
			ids = m.L.export(ids, tokens, "0")
		}

		if m.R != nil {
			ids = m.R.export(ids, tokens, "1")
		}

		tokens = tokens[:len(tokens)-1]
	}

	return ids
}

func (n *Node) equal(n2 *Node) bool {
	if n.IsTombstone != n2.IsTombstone ||
		n.MiniNodeTombstones != n2.MiniNodeTombstones ||
		len(n.MiniNodes) != len(n2.MiniNodes) {
		return false
	}

	for i := range n.MiniNodes {
		if n.MiniNodes[i].IsTombstone != n2.MiniNodes[i].IsTombstone ||
			n.MiniNodes[i].Disambiguator != n2.MiniNodes[i].Disambiguator ||
			n.MiniNodes[i].Val != n2.MiniNodes[i].Val {
			return false
		}

		if n.MiniNodes[i].L == nil && n2.MiniNodes[i].L != nil ||
			n.MiniNodes[i].L != nil && n2.MiniNodes[i].L == nil ||
			n.MiniNodes[i].R == nil && n2.MiniNodes[i].R != nil ||
			n.MiniNodes[i].R != nil && n2.MiniNodes[i].R == nil {
			return false
		}

		if n.MiniNodes[i].L != nil {
			if !n.MiniNodes[i].L.equal(n2.MiniNodes[i].L) {
				return false
			}
		}

		if n.MiniNodes[i].R != nil {
			if !n.MiniNodes[i].R.equal(n2.MiniNodes[i].R) {
				return false
			}
		}
	}

	return true
}

func (n *Node) init(nodes, currNodes, h, currH int) int {
	currH++
	n.MiniNodes = append(n.MiniNodes, &MiniNode{
		IsTombstone:   false,
		Val:           0,
		Disambiguator: "",
		L:             nil,
		R:             nil,
	})

	currNodes++
	if currNodes >= nodes {
		return currNodes
	}

	if (currH + 1) <= h {
		n.MiniNodes[0].L = &Node{}
		currNodes = n.MiniNodes[0].L.init(nodes, currNodes, h, currH)

		if currNodes >= nodes {
			return currNodes
		}

		n.MiniNodes[0].R = &Node{}
		currNodes = n.MiniNodes[0].R.init(nodes, currNodes, h, currH)
	}

	return currNodes
}

func (n *Node) insert(id idset.ID, idx int) {
	if idx == (len(id.Tokens) - 1) {
		nextM := getNextMiniNode(n, id.Tokens[idx].Disambiguator)
		if nextM == nil {
			n.IsTombstone = false
			appendMiniNode(n, id.Val, id.Tokens[idx].Disambiguator)
		} else {
			nextM.Val = id.Val
		}
		return
	}

	nextM := getNextMiniNode(n, id.Tokens[idx].Disambiguator)
	if nextM == nil {
		appendMiniNode(n, 0, id.Tokens[idx].Disambiguator)
		nextM = getNextMiniNode(n, id.Tokens[idx].Disambiguator)
	}

	if id.Tokens[idx+1].Tag == "0" {
		if nextM.L == nil {
			nextM.L = &Node{}
		}
		nextM.L.insert(id, idx+1)
		return
	}

	if nextM.R == nil {
		nextM.R = &Node{}
	}
	nextM.R.insert(id, idx+1)
}

func getNextMiniNode(n *Node, dis string) *MiniNode {
	for _, m := range n.MiniNodes {
		if m.Disambiguator == dis {
			return m
		}
	}

	return nil
}

func appendMiniNodeOrCreateNode(n *Node, val int32, dis string) *Node {
	if n == nil || len(n.MiniNodes) == 0 {
		return &Node{
			MiniNodes: []*MiniNode{
				{
					Val:           val,
					Disambiguator: dis,
				},
			},
		}
	}

	appendMiniNode(n, val, dis)
	return n
}

func appendMiniNode(n *Node, val int32, dis string) {
	if len(n.MiniNodes) == 0 {
		n.MiniNodes = append(n.MiniNodes, &MiniNode{
			Val:           val,
			Disambiguator: dis,
		})
		return
	}

	// TODO: implement as list
	if dis < n.MiniNodes[0].Disambiguator {
		n.MiniNodes = append([]*MiniNode{
			{
				Val:           val,
				Disambiguator: dis,
			},
		}, n.MiniNodes...)
		return
	}

	for i := 1; i < len(n.MiniNodes); i++ {
		if n.MiniNodes[i].Disambiguator > dis && n.MiniNodes[i-1].Disambiguator < dis {
			n.MiniNodes = append(n.MiniNodes[:i], append([]*MiniNode{
				{
					Val:           val,
					Disambiguator: dis,
				},
			}, n.MiniNodes[i:]...)...)
			return
		}
	}

	n.MiniNodes = append(n.MiniNodes, &MiniNode{
		Val:           val,
		Disambiguator: dis,
	})

	return
}

func (n *Node) newID(val int32, l, r idset.ID, lIdx, rIdx int, dis string, tokens []idset.IDToken) idset.ID {
	nextM := getNextMiniNode(n, l.Tokens[lIdx].Disambiguator)

	if lIdx == (len(l.Tokens)-1) && rIdx < (len(r.Tokens)-1) {
		if l.Tokens[lIdx].Disambiguator == r.Tokens[rIdx].Disambiguator {
			// l is ancestor of r
			return n.followLeftRoot(val, r, rIdx, dis, tokens)
		}

		tokens = append(tokens, l.Tokens[lIdx])
		tokens = append(tokens, idset.IDToken{
			Tag:           "1",
			Disambiguator: dis,
		})

		nextM.R = appendMiniNodeOrCreateNode(nextM.R, val, dis)

		return idset.NewID(tokens, val)
	}

	if rIdx == (len(r.Tokens)-1) && lIdx < (len(l.Tokens)-1) {
		// r is ancestor of l OR this is just last else
		return n.followRightRoot(val, l, lIdx, dis, tokens)
	}

	if l.Tokens[lIdx].Tag == r.Tokens[rIdx].Tag &&
		l.Tokens[lIdx].Disambiguator == r.Tokens[rIdx].Disambiguator {
		tokens = append(tokens, r.Tokens[rIdx])
	}

	if lIdx == (len(l.Tokens)-1) && rIdx == (len(r.Tokens)-1) {
		if l.Tokens[lIdx].Disambiguator != r.Tokens[rIdx].Disambiguator {
			tokens = append(tokens, l.Tokens[lIdx])
		}

		tokens = append(tokens, idset.IDToken{
			Tag:           "1",
			Disambiguator: dis,
		})

		nextM.R = appendMiniNodeOrCreateNode(nextM.R, val, dis)

		return idset.NewID(tokens, val)
	}

	if l.Tokens[lIdx].Disambiguator == r.Tokens[rIdx].Disambiguator &&
		l.Tokens[lIdx+1].Tag == r.Tokens[rIdx+1].Tag {
		if l.Tokens[lIdx+1].Tag == "0" {
			return nextM.L.newID(val, l, r, lIdx+1, rIdx+1, dis, tokens)
		}

		return nextM.R.newID(val, l, r, lIdx+1, rIdx+1, dis, tokens)
	}

	return n.followRightRoot(val, l, lIdx, dis, tokens)
}

func (n *Node) followRightRoot(val int32, id idset.ID, idx int, dis string, tokens []idset.IDToken) idset.ID {
	tokens = append(tokens, id.Tokens[idx])

	nextM := getNextMiniNode(n, id.Tokens[idx].Disambiguator)

	if idx == (len(id.Tokens) - 1) {
		tokens = append(tokens, idset.IDToken{
			Tag:           "1",
			Disambiguator: dis,
		})

		nextM.R = appendMiniNodeOrCreateNode(nextM.R, val, dis)

		return idset.NewID(tokens, val)
	}

	idx++

	if id.Tokens[idx].Tag == "0" {
		return nextM.L.followRightRoot(val, id, idx, dis, tokens)
	}

	return nextM.R.followRightRoot(val, id, idx, dis, tokens)
}

func (n *Node) followLeftRoot(val int32, id idset.ID, idx int, dis string, tokens []idset.IDToken) idset.ID {
	tokens = append(tokens, id.Tokens[idx])

	nextM := getNextMiniNode(n, id.Tokens[idx].Disambiguator)

	if idx == (len(id.Tokens) - 1) {
		tokens = append(tokens, idset.IDToken{
			Tag:           "0",
			Disambiguator: dis,
		})

		nextM.L = appendMiniNodeOrCreateNode(nextM.L, val, dis)

		return idset.NewID(tokens, val)
	}

	idx++

	if id.Tokens[idx].Tag == "0" {
		return nextM.L.followLeftRoot(val, id, idx, dis, tokens)
	}

	return nextM.R.followLeftRoot(val, id, idx, dis, tokens)
}

func (n *Node) isRemoved(id idset.ID, idx int) bool {
	nextM := getNextMiniNode(n, id.Tokens[idx].Disambiguator)

	if idx == (len(id.Tokens) - 1) {
		return nextM.IsTombstone
	}

	idx++
	if id.Tokens[idx].Tag == "0" {
		return nextM.L.isRemoved(id, idx)
	}

	return nextM.R.isRemoved(id, idx)
}

func (n *Node) remove(id idset.ID, idx int) bool {
	nextM := getNextMiniNode(n, id.Tokens[idx].Disambiguator)
	if nextM == nil {
		appendMiniNode(n, id.Val, id.Tokens[idx].Disambiguator)
		nextM = getNextMiniNode(n, id.Tokens[idx].Disambiguator)
	}

	if idx == (len(id.Tokens) - 1) {
		if !nextM.IsTombstone {
			nextM.IsTombstone = true

			n.MiniNodeTombstones++
			if n.MiniNodeTombstones == len(n.MiniNodes) {
				n.IsTombstone = true
			}

			return true
		}

		return false
	}

	idx++
	if id.Tokens[idx].Tag == "0" {
		if nextM.L == nil {
			nextM.L = &Node{}
		}
		return nextM.L.remove(id, idx)
	}

	if nextM.R == nil {
		nextM.R = &Node{}
	}
	return nextM.R.remove(id, idx)
}

package idtree

import (
	"encoding/json"

	"gitlab.com/darkrengarius/collabed/pkg/idset"
)

type idListNode struct {
	id   idset.ID
	prev *idListNode
	next *idListNode
}

type idList struct {
	h  *idListNode
	ln int
}

func (l *idList) MarshalJSON() ([]byte, error) {
	ids := l.export()
	return json.Marshal(&ids)
}

func (l *idList) UnmarshalJSON(data []byte) error {
	var ids []idset.ID
	if err := json.Unmarshal(data, &ids); err != nil {
		return err
	}

	for _, id := range ids {
		l.add(id)
	}

	return nil
}

func newIDList() *idList {
	return &idList{}
}

func (l *idList) export() []idset.ID {
	var ids []idset.ID
	for h := l.h; h != nil; h = h.next {
		ids = append(ids, h.id)
	}

	return ids
}

func (l *idList) add(id idset.ID) *idListNode {
	l.ln++

	node := &idListNode{
		id:   id,
		prev: nil,
		next: nil,
	}

	if l.h == nil {
		l.h = node
		return node
	}

	if id.Before(l.h.id) {
		node.next = l.h
		l.h.prev = node
		l.h = node
		return node
	}

	if l.h.next == nil && id.After(l.h.id) {
		l.h.next = node
		node.prev = l.h
		return node
	}

	var last *idListNode
	for h := l.h.next; h != nil; h = h.next {
		last = h

		if !h.prev.id.Before(id) || !h.id.After(id) {
			continue
		}

		node.prev = h.prev
		node.next = h
		h.prev.next = node
		h.prev = node

		return node
	}

	last.next = node
	node.prev = last

	return node
}

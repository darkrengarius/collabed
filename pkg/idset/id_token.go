package idset

type IDToken struct {
	Tag           string
	Disambiguator string
}

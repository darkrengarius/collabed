package idset

import (
	"strconv"
	"sync/atomic"
)

type Tiebreaker struct {
	counter  uint64
	clientID string
}

func NewTiebreaker(clientID string) *Tiebreaker {
	return &Tiebreaker{
		clientID: clientID,
	}
}

func (t *Tiebreaker) NextDisambiguator() string {
	return strconv.FormatUint(t.incCounter(), 10) + ":" + t.clientID
}

func (t *Tiebreaker) incCounter() uint64 {
	return atomic.AddUint64(&t.counter, 1)
}

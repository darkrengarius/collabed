package idset

var EmptyID = ID{}

type ID struct {
	Key         string
	Tokens      []IDToken
	Val         int32
	IsTombstone bool
}

func NewID(tokens []IDToken, val int32) ID {
	id := ID{
		Tokens: tokens,
		Val:    val,
	}
	id.Key = id.String()

	return id
}

func (id ID) String() string {
	s := ""
	for _, t := range id.Tokens {
		s += t.Tag + ":" + t.Disambiguator + "-"
	}

	return s[:len(s)-1]
}

func (id ID) Equals(id2 ID) bool {
	if len(id.Tokens) != len(id2.Tokens) {
		return false
	}

	for i := range id.Tokens {
		if id.Tokens[i].Tag != id2.Tokens[i].Tag ||
			id.Tokens[i].Disambiguator != id2.Tokens[i].Disambiguator {
			return false
		}
	}

	return true
}

func (id ID) After(id2 ID) bool {
	return !id.Before(id2)
}

func (id ID) Before(id2 ID) bool {
	i := 0
	for ; i < len(id.Tokens) && i < len(id2.Tokens); i++ {
		if id.Tokens[i].Tag == id2.Tokens[i].Tag {
			if id.Tokens[i].Disambiguator != id2.Tokens[i].Disambiguator {
				return id.Tokens[i].Disambiguator < id2.Tokens[i].Disambiguator
			}

			continue
		}

		return id.Tokens[i].Tag < id2.Tokens[i].Tag
	}

	if i == len(id.Tokens) && i == len(id2.Tokens) {
		return id.Tokens[len(id.Tokens)-1].Disambiguator < id2.Tokens[len(id2.Tokens)-1].Disambiguator
	}

	if len(id.Tokens) < len(id2.Tokens) {
		return id2.Tokens[i].Tag == "1"
	}

	return id.Tokens[i].Tag == "0"
}

func (id ID) IsEmpty() bool {
	return len(id.Tokens) == 0
}

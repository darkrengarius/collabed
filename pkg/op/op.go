package op

import "gitlab.com/darkrengarius/collabed/pkg/idset"

// Op is an operation on the replicated state.
type Op struct {
	Type Type
	ID   idset.ID
}

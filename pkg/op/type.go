package op

// Type is an operation type.
type Type int

const (
	TypeInsert Type = iota + 1
	TypeUpdate
	TypeDelete
)

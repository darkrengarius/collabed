# collabed

This basically a treedoc CRDT implementation for collaborative editing. At this stage it lacks GC and balancing. Original treedoc structure does not support update operations. Update is implemented as a combination of remove and insert operations, which generates new identifier for an item but in the same position.

Server and clients are implemented simply as goroutines sending operations over channels to each other.

# How to Run

Run `go run main.go`.
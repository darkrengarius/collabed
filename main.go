package main

import (
	"fmt"
	"strconv"
	"sync"
	"time"

	"gitlab.com/darkrengarius/collabed/pkg/op"

	"gitlab.com/darkrengarius/collabed/pkg/idset"
	"gitlab.com/darkrengarius/collabed/pkg/idset/idtree"
)

type OPWithClientID struct {
	o    op.Op
	clID string
}

var (
	srvState    = idtree.NewTreeIDSetInit(5)
	srvCh       = make(chan OPWithClientID, 1000)
	clientsMx   sync.Mutex
	clients     = make(map[string]chan op.Op)
	clWG2       sync.WaitGroup
	srvWG       sync.WaitGroup
	localStates = make(map[string]*idtree.TreeIDSet)
	clWG3       sync.WaitGroup
	rnd         = idtree.NewRand()
)

func randomInsert(clID string, state *idtree.TreeIDSet, tb *idset.Tiebreaker) {
	id := state.RandomInsert(rnd, tb.NextDisambiguator())
	o := op.Op{
		Type: op.TypeInsert,
		ID:   id,
	}

	ocl := OPWithClientID{
		o:    o,
		clID: clID,
	}

	srvCh <- ocl
}

func randomUpdate(clID string, state *idtree.TreeIDSet, tb *idset.Tiebreaker) bool {
	rmID, upID := state.RandomUpdate(rnd, tb.NextDisambiguator())
	if rmID.IsEmpty() {
		return false
	}

	rmOp := OPWithClientID{
		o: op.Op{
			Type: op.TypeDelete,
			ID:   rmID,
		},
		clID: clID,
	}
	upOp := OPWithClientID{
		o: op.Op{
			Type: op.TypeInsert,
			ID:   upID,
		},
		clID: clID,
	}

	srvCh <- rmOp
	srvCh <- upOp

	return true
}

func randomDelete(clID string, state *idtree.TreeIDSet) bool {
	id := state.RandomDelete(rnd)
	if id.IsEmpty() {
		return false
	}

	delO := op.Op{
		Type: op.TypeDelete,
		ID:   id,
	}

	ocl := OPWithClientID{
		o:    delO,
		clID: clID,
	}

	srvCh <- ocl

	return true
}

func runServer() {
	defer srvWG.Done()

	for ocl := range srvCh {
		srvState.Replay(ocl.o)

		clientsMx.Lock()
		for clID, cl := range clients {
			if clID == ocl.clID {
				continue
			}

			cl <- ocl.o
		}
		clientsMx.Unlock()
	}

	clientsMx.Lock()
	for _, clCH := range clients {
		close(clCH)
	}
	clientsMx.Unlock()
}

func cloneSrvState() *idtree.TreeIDSet {
	return idtree.Import(srvState.Export())
}

func runClient(clID string) {
	localState := cloneSrvState()
	tb := idset.NewTiebreaker(clID)

	ch := make(chan op.Op, 1000)
	clientsMx.Lock()
	localStates[clID] = localState
	clients[clID] = ch
	clientsMx.Unlock()

	go func() {
		const opsCount = 30

		for i := 0; i < opsCount; i++ {
			ms := rnd.Intn(200) + 1
			time.Sleep(time.Duration(ms) * time.Millisecond)

			opType := op.Type(rnd.Intn(3) + 1)
			switch opType {
			case op.TypeInsert:
				randomInsert(clID, localState, tb)
			case op.TypeUpdate:
				if !randomUpdate(clID, localState, tb) {
					randomInsert(clID, localState, tb)
				}
			case op.TypeDelete:
				if !randomDelete(clID, localState) {
					randomInsert(clID, localState, tb)
				}
			}
		}

		clWG2.Done()
	}()

	for o := range ch {
		localState.Replay(o)
	}
	clWG3.Done()
}

func main() {
	const clsCount = 20

	clWG2.Add(clsCount)
	clWG3.Add(clsCount)

	srvWG.Add(1)
	go runServer()

	for i := 0; i < clsCount; i++ {
		go runClient("CLID" + strconv.Itoa(i))
	}

	clWG2.Wait()
	close(srvCh)
	srvWG.Wait()
	clWG3.Wait()

	fmt.Println("Comparing states")

	allEqual := true
	for clID, localState := range localStates {
		for clID2, localState2 := range localStates {
			if clID == clID2 {
				continue
			}

			if !localState.Equal(localState2) {
				allEqual = false
				fmt.Printf("States %s and %s not equal\n", clID, clID2)
			}
		}
	}

	if allEqual {
		fmt.Println("All states are equal")
	}
}
